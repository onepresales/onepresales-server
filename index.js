const express = require('express');
const app = express();
const http = require('http');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const onepresalesServerAuth = require("onepresales-server-auth");

// Express 4.0
app.use(bodyParser.json({ limit: '10mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '10mb' }));

app.use(cookieParser());


function start( options ) {

	onepresalesServerAuth(app, options);

	if ( options.secure ) {
		console.log("Sorry HTTPS is not yet implemented.");
		return;
	}

	app.use(express.static(options.staticRoot));

	http.createServer(app).listen(options.port, function(){
    	console.log('App listening on port '+options.port+' (HTTP)');
	});
}

/*start({
	secure: false,
	port: 3000,
	staticRoot: path.resolve(__dirname, 'public')
});*/

module.exports = {
	start: start,
	app: app,
};